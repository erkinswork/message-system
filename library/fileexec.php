<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 2:14 AM
 */

class FileExec{

    protected $filename;

    protected $handle;

    protected $locked;

    protected $extension;

    protected $raw;

    protected $content;

    protected $settings = [];

    static protected $instances = [];

    public static function instance($filename)
    {
        if (!is_string($filename) && $filename) {
            throw new \InvalidArgumentException('Filename should be non-empty string');
        }
        if (!isset(static::$instances[$filename])) {
            static::$instances[$filename] = new static;
            static::$instances[$filename]->init($filename);
        }
        return static::$instances[$filename];
    }

    public function unlock()
    {
        if (!$this->handle) {
            return false;
        }
        if ($this->locked) {
            flock($this->handle, LOCK_UN);
            $this->locked = null;
        }
        fclose($this->handle);
        $this->handle = null;

        return true;
    }

    public function free()
    {
        if ($this->locked) {
            $this->unlock();
        }
        $this->content = null;
        $this->raw = null;

        unset(static::$instances[$this->filename]);
    }

    protected function init($filename)
    {
        $this->filename = $filename;
    }

    public function lock($block = true)
    {
        if (!$this->handle) {
            if (!$this->mkdir(dirname($this->filename))) {
                throw new \RuntimeException('Creating directory failed for ' . $this->filename);
            }
            $this->handle = @fopen($this->filename, 'cb+');
            if (!$this->handle) {
                $error = error_get_last();

                throw new \RuntimeException("Opening file for writing failed on error {$error['message']}");
            }
        }
        $lock = $block ? LOCK_EX : LOCK_EX | LOCK_NB;
        return $this->locked = $this->handle ? flock($this->handle, $lock) : false;
    }

    public function save($data = null)
    {
        if ($data !== null) {
            $this->content($data);
        }

        if (!$this->locked) {

            if (!$this->lock()) {
                throw new \RuntimeException('Obtaining write lock failed on file: ' . $this->filename);
            }
            $lock = true;
        }

        if (@ftruncate($this->handle, 0) === false || @fwrite($this->handle, $this->raw()) === false) {
            $this->unlock();
            throw new \RuntimeException('Saving file failed: ' . $this->filename);
        }

        if (isset($lock)) {
            $this->unlock();
        }

        @touch(dirname($this->filename));
    }

    protected function mkdir($dir)
    {

        if (!@is_dir($dir)) {
            $success = @mkdir($dir, 0777, true);

            if (!$success) {
                $error = error_get_last();

                throw new \RuntimeException("Creating directory '{$dir}' failed on error {$error['message']}");
            }
        }

        return true;
    }

    public function locked()
    {
        return $this->locked;
    }

    public function content($var = null)
    {
        if ($var !== null) {
            $this->content = $this->check($var);

            // Update RAW, too.
            $this->raw = $this->encode($this->content);

        } elseif ($this->content === null) {
            // Decode RAW file.
            try {
                $this->content = $this->decode($this->raw());
            } catch (\Exception $e) {
                throw new \RuntimeException(sprintf('Failed to read %s: %s', $this->filename, $e->getMessage()), 500, $e);
            }
        }

        return $this->content;
    }

    protected function check($var)
    {
        return (string) $var;
    }

    protected function encode($var)
    {
        return (string) $var;
    }

    public function raw($var = null)
    {
        if ($var !== null) {
            $this->raw = (string) $var;
            $this->content = null;
        }

        if (!is_string($this->raw)) {
            $this->raw = $this->load();
        }

        return $this->raw;
    }

}