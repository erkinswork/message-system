<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 1:21 AM
 */

require_once(get_main_path() . '/config.php');

function get_main_path(){

    return getcwd();

}

function get_template($array){

    $config = new Config();
    $template_name =  $config->get_template_name();
    get_template_part(array('part' => 'header', 'temp_name' => $template_name));
    get_template_part(array('part' => 'body', 'temp_name' => $template_name));
    get_body_section($array, $template_name);
    get_template_part(array('part' => 'footer', 'temp_name' => $template_name));

}

function get_template_part($settings){

    if($settings['part'] == 'header'){

        require_once(get_main_path() . '/framework/views/templates/'. $settings['temp_name'] . '/header.php');

    } else if($settings['part'] == 'footer'){

        require_once(get_main_path() . '/framework/views/templates/'. $settings['temp_name'] . '/footer.php');

    }

}

function get_body_section($array, $temp_name){

    require_once(get_main_path() . '/framework/views/templates/'. $temp_name . '/body.php');
    get_body($array);

}

function get_required_scripts($script_name){

    return '<script id="" type="text/javascript" src="'.base_url().'/js/'.$script_name.'.js"></script>';

}

function get_required_styles($style_name){

    return '<link href="'.base_url().'/css/'.$style_name.'.css" rel="stylesheet" >';

}

function base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){

    if (isset($_SERVER['HTTP_HOST'])) {
        $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
        $hostname = $_SERVER['HTTP_HOST'];
        $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
        $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
        $core = $core[0];
        $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
        $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
        $base_url = sprintf( $tmplt, $http, $hostname, $end );
    }
    else $base_url = 'http://localhost/';

    if ($parse) {
        $base_url = parse_url($base_url);
        if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
    }

    return $base_url;
}

function get_logo(){

    return '<img src = "'. base_url() .'images/logo.png" >';

}

function get_html_parts($part){

    $config = new Config();
    require_once(getcwd(). '/framework/views/templates/'.$config->get_template_name().'/parts/'.$part . '.php');

}