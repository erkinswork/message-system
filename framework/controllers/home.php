<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 3:17 AM
 */

require_once(getcwd(). '/library/functions.php');
require_once(getcwd(). '/framework/models/homemodel.php');


class Home{

    function __construct()
    {

        if($_SERVER["REQUEST_METHOD"]=="GET" && !empty($_GET['check_db'])){

            if(!empty($_GET['check_db'])){
                $this->get_one_message();
            }
            die();

        } else {
            get_template($this->get_messages());
            $this->save_messages();
        }

    }

    public function get_messages(){

        $home = new HomeModel();
        return $home->get_message();

        get_html_parts('messages');
        get_message_block($home->get_message());

    }

    public function save_messages(){

        if($_SERVER["REQUEST_METHOD"]=="GET"){

            if(!empty($_GET)){

                $name = $_GET['name'];
                $email = $_GET['email'];
                $comment = $_GET['comment'];
                $settings = array(
                    'id'=>'',
                    'name'=>$name,
                    'email' => $email,
                    'comment' => $comment
                );

                $home = new HomeModel();
                $home->save_message($settings);

            }

        }

    }

    public function get_one_message(){

        $home = new HomeModel();
        $json = json_encode(array('slider'=>$home->get_one_message(), 'status' => 'success'));
        echo $_GET['callback'] . '(' .$json.')';

    }
}

new Home();