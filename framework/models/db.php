<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 5:50 AM
 */

require_once(getcwd(). '/config.php');

class db{

    public function get_db (){

        $config = new Config();
        return mysqli_connect($config->get_db_host(), $config->get_db_login(), $config->get_db_pass(), $config->get_db_name());

    }

}