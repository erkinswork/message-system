<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 5:48 AM
 */

require_once(getcwd(). '/framework/models/db.php');

class MainController{

    protected $db_connection;

    public function __construct()
    {

        $db = new db();
        $this->db_connection = $db->get_db();

    }

    public function save_message($settings){

        $date = date("Y/m/d");
        $sql = "INSERT INTO messages (id, name, email, comment, time)
        VALUES ('".$settings['id']."', '".$settings['name']."', '".$settings['email']."', '".$settings['comment']."', '".$date."')";

        if (mysqli_query($this->db_connection, $sql)) {

            return true;

        } else {

            return false;

        }

        print_r($settings);
        die();

    }

    public function get_message(){

        $sql="SELECT * FROM messages ORDER BY time";
        $result=mysqli_query($this->db_connection, $sql);

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
        mysqli_free_result($result);

    }

    public function get_one_message(){

        $sql="SELECT * FROM messages  ORDER BY id DESC LIMIT 1 ";
        $result=mysqli_query($this->db_connection, $sql);

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
        mysqli_free_result($result);

    }

}