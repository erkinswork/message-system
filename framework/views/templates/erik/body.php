<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 8:31 AM
 */

require_once(getcwd(). '/library/functions.php');

function get_body($settings){
 ?>
    <section class="mainbar">

        <div class="erk_messages_text">
            <h2>Выводим комментарии</h2>
        </div>

        <?php
        get_html_parts('messages');
        get_message_block($settings);

?>
    </section>
<?php } ?>