<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 3:15 AM
 */
require_once(getcwd(). '/library/functions.php');
?>

    </div>
        <footer class="footer">
            <div class="container">


                <div class="row">
                    <div class="col-md-6">
                        <div class="footer_logo_section">
                            <a href="<?= base_url() ?>"><?= get_logo() ?></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="social_icons_section">
                            <a href="#"><i class="icon-social-facebook"></i></a>
                            <a href="#"><i class="icon-social-twitter"></i></a>
                            <a href="#"><i class="icon-social-youtube"></i></a>
                            <a href="#"><i class="icon-social-dropbox"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
