<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 3:15 AM
 */
require_once(getcwd(). '/library/functions.php');

?>

<html>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?= get_required_styles('simple-line-icons') ?>
        <?= get_required_styles('bootstrap') ?>
        <?= get_required_styles('main') ?>

        <?= get_required_scripts('jquery-3.3.1.min') ?>
        <?= get_required_scripts('jquery.validate.min') ?>
        <?= get_required_scripts('bootstrap') ?>
        <?= get_required_scripts('main') ?>

    </head>
<body>
    <div class="erk_header">
        <div class="container">
            <div class="row align-items-center erk_">
                <div class="logo_section">
                    <a href="<?= base_url() ?>"><?= get_logo() ?></a>
                </div>
                <div class="feature_section col-md-12">
                    <img src = "<?= base_url() ?>images/contact_icon.png">
                </div>
                <div class="form_section col-md-12">
                    <form action="" method="POST">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="validationServer01" class="required">Имя</label>
                                        <input type="text" name="name" class="form-control" id="" placeholder="Введите Имя" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="validationServer02" class="required">E-mail</label>
                                        <input type="text" name="email" class="form-control" id="" placeholder="Введите ваш Email" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1" class="required">Комментарий</label>
                                        <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="erk_button_section">
                            <button class="btn erk_btn" type="submit">Записать</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="erk_body">