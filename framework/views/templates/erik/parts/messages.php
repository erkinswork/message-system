<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 7:57 AM
 */

function get_message_block($settings){
?>

<div class="container">
    <div class="row">

    <?php
    $i = 0;
    foreach($settings as $value){
        $green_block = '';

        if($i%2) {

            $green_block = 'erk_green_block';

        }

    ?>
        <div class="col-md-4">
            <div class="erk_message_block <?= $green_block ?>">
                <div class="erk_message_name">
                    <h3><?php echo $value['name']?></h3>
                </div>
                <div class="erk_message_body">
                    <div class="erk_message_email">
                        <h4><?php echo $value['email']?><h4>
                    </div>
                    <div class="erk_message_comment">
                        <?php echo $value['comment']?>
                    </div>
                </div>
            </div>
        </div>
    <?php
        $i++;
    }
?>
    </div>
</div>
<?php } ?>
