jQuery(function($){
    $(document).ready(function() {

        $(".form_section form").validate({

            errorClass: "erk_form_error",
            errorPlacement: function(error,elemtn){},

            rules: {

                "name" : {required: true},
                "email" : {required: true, email: true},
                "comment" : {required: true}

            },

            messages: {

                "name" : "",
                "email" : "",
                "comment" : ""

            },

            submitHandler: function(form){
                $.ajax({

                    data: $(".form_section form").serialize(),
                    url: window.location.href,
                    beforeSend: function(){
                        var message_block = '<div class="col-md-4">' +
                            '<div class="erk_message_block erk_preload">' +
                            ' <div class="erk_message_name">' +
                            '<h3>...</h3>' +
                            '</div>' +
                            '<div class="erk_message_body">' +
                            '<div class="erk_message_email">' +
                            '<h4>...</h4>' +
                            '</div>' +
                            '<div class="erk_message_comment">...' +
                            '</div>' +
                            '</div>' +
                            '<div class="erk_overflow">' +
                            '<div class="preloader_section">' +
                            ' <div class="preloader">' +
                            '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $(".mainbar .row").prepend(message_block);
                    },
                    success: function (data) {
                        $.ajax({
                            url: window.location.href + 'index.php?&check_db=1&callback',
                            dataType: "jsonp",
                            success: function(data){


                                setTimeout(function(){
                                    $('.erk_preload').parent().remove();


                                    var erk_name = data.slider[0].name;
                                    var erk_email = data.slider[0].email;
                                    var erk_comment = data.slider[0].comment;
                                    var message_block = '<div class="col-md-4">' +
                                        '<div class="erk_message_block">' +
                                        ' <div class="erk_message_name">' +
                                        '<h3>'+erk_name+'</h3>' +
                                        '</div>' +
                                        '<div class="erk_message_body">' +
                                        '<div class="erk_message_email">' +
                                        '<h4>'+erk_email+'</h4>' +
                                        '</div>' +
                                        '<div class="erk_message_comment">' +
                                        erk_comment +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>';

                                    $(".mainbar .row").prepend(message_block);

                                    var i = 0;
                                    $(".erk_message_block").each(function(){
                                        $(".erk_message_block").removeClass("erk_green_block");

                                        $(".erk_message_block:odd").addClass("erk_green_block");

                                        i++;
                                    });

                                }, 3000);

                            },
                            error: function (data) {

                            }

                        });

                    },
                    error: function(data){

                    }

                })

            }

        });

        $('<span style="color:red;">*</span>').insertAfter('.required');

    });

});
