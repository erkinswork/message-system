<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);

if(file_exists(getcwd() . '/library/functions.php')){

    require_once(getcwd() . '/library/functions.php');

}

if(file_exists(getcwd() . '/library/fileexec.php')){

    require_once(getcwd() . '/library/fileexec.php');

}

if(file_exists(getcwd() . '/library/AltoRouter.php')){

    require_once(getcwd() . '/library/AltoRouter.php');

}

require getcwd() . '/framework/controllers/home.php';

$directory = "css";

require get_main_path() . "/vendor/leafo/scssphp/scss.inc.php";

use Leafo\ScssPhp\Compiler;
$scss = new Compiler();
$scss->setImportPaths('css/scss/');

$css =  $scss->compile('@import "main.scss";');
// $css =  $scss->compile('@import "../vendor/twbs/bootstrap/scss";');

////////////////////////////////////////////////////////////////////////////////////
$file = FileExec::instance(get_main_path() . '/css/main.css');

try {
    $file->lock(false);
} catch (\Exception $e) {

}

if ($file->locked() === false) {

    return false;
}
/////////////////////////////////////////////////////////////////////////////////////


$file->save($css);
$file->unlock();
$file->free();

// scss_server::serveFrom($directory);

?>
