<?php
/**
 * Created by PhpStorm.
 * User: erkin
 * Date: 12/12/2018
 * Time: 3:18 AM
 */

class Config{

    protected $template_name = 'erik';

    protected $db_host = 'localhost';

    protected $db_name = 'erk_messages';

    protected $db_login = 'root';

    protected $db_pass = '';

    function get_template_name(){
        return $this->template_name;
    }

    function get_db_host(){

        return $this->db_host;

    }

    function get_db_name(){

        return $this->db_name;

    }

    function get_db_login(){

        return $this->db_login;

    }

    function get_db_pass(){

        return $this->db_pass;

    }

}